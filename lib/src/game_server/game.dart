import 'package:proto_socket_game_server/src/generated/game_service.pb.dart';
import 'package:protobuf/protobuf.dart';
import 'package:stream_channel/stream_channel.dart';

abstract class Game {
   join(GeneratedMessage message, Future<StreamChannel<dynamic>> channel);
}
