import 'game_server.dart';

abstract class HttpHandler<T, R> {
  String get route;

  R handle(T value, GameService gameService);
}
