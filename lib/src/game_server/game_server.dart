import 'dart:async';
import 'dart:mirrors';
import 'dart:typed_data';

import 'package:async/async.dart';
import 'package:proto_socket_game_server/src/generated/game_service.pb.dart';
import 'package:protobuf/protobuf.dart';
import 'package:shelf/shelf.dart' as shelf;
import 'package:shelf/shelf.dart';
import 'package:shelf/shelf_io.dart' as shelf_io;
import 'package:shelf_router/shelf_router.dart';
import 'package:shelf_web_socket/src/web_socket_handler.dart';
import 'package:stream_channel/stream_channel.dart';
import 'package:uuid/uuid.dart';

import 'HttpHandler.dart';
import 'game_factory.dart';

/// Use as a server interface to handle game communication, and translate incomming messages to gameMessages
/// It is responsible for creating games and initiating websocket connections
class GameService {
  List<Type> _playerMessages;

  GameFactory factory;

  List<HttpHandler> staticHandlers;

  Map<String, Function> _messageParsers;
  Type joinMessage;

  String allowed_origins;
  Map<String, Completer<StreamChannel>> pendingSockets;
  Set<String> pendingSocketsId;

  GameService(
    this._playerMessages,
    this.staticHandlers,
    this.factory,
    this.joinMessage,
      this.allowed_origins,
  ) {
    pendingSockets =Map();
    pendingSocketsId = Set();
    staticHandlers.add(GameCreator(factory));
    staticHandlers.add(JoinGame());
    _messageParsers = {};
    _playerMessages.forEach((element) {
      final reflection = reflectClass(element);
      _messageParsers[element.toString()] =
          (List<int> values) => reflection.newInstance(#fromBuffer, [values]).reflectee;
    });
  }

  Future<void> serve(url, port) async {
    final router = Router();
    staticHandlers.forEach((element) {
      router.all(element.route, (shelf.Request r) async {
        if (r.method == 'POST') {
          GeneratedMessage answer = element.handle(
              parseMessage((await r.read().toList())
                  .reduce((value, element) => element..addAll(value))),
              this);
          return shelf.Response.ok(serializeMessage(answer));
        } else {
          return shelf.Response.ok('');
        }
      });
    });

    router.all("/websocket", WebSocketHandler(handleWebSocket,pendingSocketsId,null,null).handle);
    // for OPTIONS (preflight) requests just add headers and an empty response
    final _headers = {'Access-Control-Allow-Origin': this.allowed_origins,
      'Content-Type': 'application/x-protobuf',"Access-Control-Allow-Headers":"Content-Type"};

    // for OPTIONS (preflight) requests just add headers and an empty response
    Response _options(Request request) => (request.method == 'OPTIONS') ? new Response.ok(null, headers: _headers) : null;
    Response _cors(Response response) => response.change(headers: _headers);

    var handler = const Pipeline().addMiddleware(createMiddleware(requestHandler: _options,responseHandler: _cors))
        .addMiddleware(logRequests()).addHandler(router.handler);

    await shelf_io.serve(handler, url, port);
    print('Serving gameService on ${url}:${port}');
  }

  handleWebSocket(StreamChannel webSocketChannel,protocol) async {
    final transformed = webSocketChannel.transform(StreamChannelTransformer(
        StreamTransformer.fromHandlers(
        handleData: (data, sink) {
          sink.add(parseMessage(data));
        }
    ),StreamSinkTransformer.fromHandlers(handleData: (data,sink ){
      sink.add(serializeMessage(data));
    },))
    );
    pendingSockets[protocol].complete(transformed);
    pendingSocketsId.remove(protocol);
  }

  GeneratedMessage parseMessage(List<int> list) {
    final gm = GameMessage.fromBuffer(list);
    return _messageParsers[gm.messageType](gm.payload);
  }

  Uint8List serializeMessage(GeneratedMessage answer) {
    return (GameMessage()
          ..messageType = (answer.runtimeType.toString())
          ..payload = (answer.writeToBuffer()))
        .writeToBuffer();
  }

  GeneratedMessage playerJoin(event) {
    final socketId = Uuid().v4();
    pendingSocketsId.add(socketId);
    pendingSockets[socketId] = Completer();
    factory.get(event).join(event,pendingSockets[socketId].future);
    return SocketConnection()..socketId=socketId..player=true;
  }
}

class GameCreator extends HttpHandler {
  GameFactory factory;

  GameCreator(this.factory);

  @override
  GeneratedMessage handle(value, GameService gameService) {
    return factory.init(value);
  }

  @override
  String get route => '/create';
}

class JoinGame extends HttpHandler{

  @override
  handle(value, GameService gameService) {
    return gameService.playerJoin(value);
  }

  @override
  String get route => "/join";


}