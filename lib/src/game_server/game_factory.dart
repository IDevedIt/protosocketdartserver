import 'package:protobuf/protobuf.dart';

import 'game.dart';

/// Responsible for creating and supplying game objects
abstract class GameFactory<CreateGameType, PlayerJoinType> {
  Game get(PlayerJoinType playerJoin);

  GeneratedMessage init(CreateGameType gameCreation);
}
