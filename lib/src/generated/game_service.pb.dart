///
//  Generated code. Do not modify.
//  source: game_service.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class GameMessage extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('GameMessage', createEmptyInstance: create)
    ..aOS(1, 'messageType', protoName: 'messageType')
    ..a<$core.List<$core.int>>(2, 'payload', $pb.PbFieldType.OY)
    ..hasRequiredFields = false
  ;

  GameMessage._() : super();
  factory GameMessage() => create();
  factory GameMessage.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GameMessage.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  GameMessage clone() => GameMessage()..mergeFromMessage(this);
  GameMessage copyWith(void Function(GameMessage) updates) => super.copyWith((message) => updates(message as GameMessage));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GameMessage create() => GameMessage._();
  GameMessage createEmptyInstance() => create();
  static $pb.PbList<GameMessage> createRepeated() => $pb.PbList<GameMessage>();
  @$core.pragma('dart2js:noInline')
  static GameMessage getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GameMessage>(create);
  static GameMessage _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get messageType => $_getSZ(0);
  @$pb.TagNumber(1)
  set messageType($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMessageType() => $_has(0);
  @$pb.TagNumber(1)
  void clearMessageType() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get payload => $_getN(1);
  @$pb.TagNumber(2)
  set payload($core.List<$core.int> v) { $_setBytes(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPayload() => $_has(1);
  @$pb.TagNumber(2)
  void clearPayload() => clearField(2);
}

class SocketConnection extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('SocketConnection', createEmptyInstance: create)
    ..aOS(1, 'socketId', protoName: 'socketId')
    ..aOB(2, 'player')
    ..hasRequiredFields = false
  ;

  SocketConnection._() : super();
  factory SocketConnection() => create();
  factory SocketConnection.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SocketConnection.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  SocketConnection clone() => SocketConnection()..mergeFromMessage(this);
  SocketConnection copyWith(void Function(SocketConnection) updates) => super.copyWith((message) => updates(message as SocketConnection));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SocketConnection create() => SocketConnection._();
  SocketConnection createEmptyInstance() => create();
  static $pb.PbList<SocketConnection> createRepeated() => $pb.PbList<SocketConnection>();
  @$core.pragma('dart2js:noInline')
  static SocketConnection getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SocketConnection>(create);
  static SocketConnection _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get socketId => $_getSZ(0);
  @$pb.TagNumber(1)
  set socketId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSocketId() => $_has(0);
  @$pb.TagNumber(1)
  void clearSocketId() => clearField(1);

  @$pb.TagNumber(2)
  $core.bool get player => $_getBF(1);
  @$pb.TagNumber(2)
  set player($core.bool v) { $_setBool(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPlayer() => $_has(1);
  @$pb.TagNumber(2)
  void clearPlayer() => clearField(2);
}

