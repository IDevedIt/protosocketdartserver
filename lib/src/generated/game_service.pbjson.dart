///
//  Generated code. Do not modify.
//  source: game_service.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

const GameMessage$json = const {
  '1': 'GameMessage',
  '2': const [
    const {'1': 'messageType', '3': 1, '4': 1, '5': 9, '10': 'messageType'},
    const {'1': 'payload', '3': 2, '4': 1, '5': 12, '10': 'payload'},
  ],
};

const SocketConnection$json = const {
  '1': 'SocketConnection',
  '2': const [
    const {'1': 'socketId', '3': 1, '4': 1, '5': 9, '10': 'socketId'},
    const {'1': 'player', '3': 2, '4': 1, '5': 8, '10': 'player'},
  ],
};

